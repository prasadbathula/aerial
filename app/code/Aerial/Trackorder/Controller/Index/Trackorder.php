<?php
namespace Aerial\Trackorder\Controller\Index;

class Trackorder extends \Magento\Framework\App\Action\Action
{

    protected $resultJsonFactory;
    protected $_resultPageFactory;
    /**
     * 
     * @param \Magento\Framework\App\Action\Context $context     *
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {

        $this->resultJsonFactory = $resultJsonFactory;
        $this->_resultPageFactory  = $resultPageFactory;
        parent::__construct($context);
    }


    public function execute()
    {
       $this->_view->loadLayout();
        $this->_view->renderLayout();
    }

        
}