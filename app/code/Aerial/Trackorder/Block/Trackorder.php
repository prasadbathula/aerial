<?php 
namespace Aerial\Trackorder\Block;

class Trackorder extends \Magento\Framework\View\Element\Template
{
    public function getApiKey()
    {
        return $this->_scopeConfig->getValue('path/to/config');
    }

    public function getApiVersion()
    {
        return $this->_scopeConfig->getValue('path/to/config');
    }
   
}
