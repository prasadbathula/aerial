/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */


var config = {
    
    paths: {
        'bootstrap' : "js/bootstrap.min", /* bootstrap 3 */
        'slick' : "js/slick.min", /* slick */
        'colorBox' : "js/jquery.colorbox" /* colorBox */
    },
    shim: {
        'bootstrap':{
            deps: ['jquery']
        },
        'slick':{
            deps: ['jquery']
        },
        'colorBox':{
            deps: ['jquery']
        }
    }
    
};