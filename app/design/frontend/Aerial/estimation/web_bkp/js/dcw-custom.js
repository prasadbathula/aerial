require(['jquery','jquery/ui', 'bootstrap', 'slick'],function ($) {
	jQuery(document).ready(function () {

		var header_H = jQuery('header').innerHeight();

		/*---------  Trusted Brands carousel  -------------*/
		// jQuery('.home_banner').slick({
		// 	autoplaySpeed: 300,
  // 		 	speed: 1800,
  // 		 	infinite: true,
		//   	slidesToShow: 5,
		//   	slidesToScroll: 1,
		//   	arrows: true,
		//   	dots: false,
		//   	infinite: true,
		//    	autoplay: true,
		//    	lazyLoad: 'ondemand' 		 	
  		 	
		// });
		jQuery('.home_banner').slick({
		  dots: true,
		  infinite: true,
		  speed: 300,
		  slidesToShow: 1,
		  adaptiveHeight: true
		});
		/*--------- category page Trusted Brands carousel  -------------*/
		jQuery('.landing_list').slick({
  		 	infinite: true,
		  	slidesToShow: 5,
		  	slidesToScroll: 1,
		  	arrows: true,
		  	dots: false,
		  	infinite: true,
		   	autoplay: true,
		   	lazyLoad: 'ondemand',
  		 	
  		 	responsive: [
		    {
		      breakpoint: 1200,
		      settings: {
		        slidesToShow: 4
		      }
		    },
		    {
		      breakpoint: 1024,
		      settings: {
		        slidesToShow: 3
		      }
		    },
		    {
		      breakpoint: 767,
		      settings: {
		        slidesToShow: 2
		      }
		    }
		  ]
		});
				

		/*------------  navigation account section  --------------*/

		if (jQuery(window).width() <= 767) {
			jQuery('.page-wrapper .page-header .panel.wrapper .panel.header > ul.header.links').appendTo('.sections.nav-sections .nav-sections-item-content');

			/*--------- header navigation  ------------*/
			jQuery('header.page-header .nav-sections .navigation > ul > li').prepend("<span class='arw'></span>");
			jQuery('nav.navigation > ul > li').click(function(e){
			  e.stopPropagation();
			});
			jQuery('nav.navigation > ul > li .arw').click(function(){
				jQuery(this).parent().toggleClass('current');
				jQuery(this).parent().siblings().removeClass('current');
			});


			/*----------  footer accordion  -----------*/
			jQuery('.inner-container.foot1 .row .col-xs-12 h3').click(function(){
				jQuery(this).parent().find('ul').slideToggle();
				jQuery(this).parent().siblings().find('ul').slideUp();
				jQuery(this).parent().toggleClass('active');
				jQuery(this).parent().siblings().removeClass('active');
			});

			jQuery('.inner-container.foot2 .row .col-xs-12 h3').click(function(){
				jQuery(this).parent().find('ul').slideToggle();
				jQuery(this).parent().siblings().find('ul').slideUp();
				jQuery(this).parent().toggleClass('active');
				jQuery(this).parent().siblings().removeClass('active');
			});
		}

       
		 /* popup designing
	    ===========================*/
	    jQuery('.hide_popup').prepend('<span class="close"></span>');
	    
	    jQuery('.popup_trigger').on('click', function(){
	        var target = "#"+jQuery(this).attr('rel');
	        jQuery(target).fadeIn();
	    });

	    jQuery('.hide_popup .close').on('click', function(){
	       jQuery('.hide_popup').hide(); 
	    });


	    // back to top---------------------------
	    if(jQuery(this).scrollTop() >= header_H) {
		    jQuery('.back_to_top').fadeIn();
		}
	    else 
	    {
	       	jQuery('.back_to_top').fadeOut();
	    }

		jQuery('.back_to_top').click(function() {
		    jQuery("html, body").animate({scrollTop: 0}, 800);
		    return false;
	   	});

      // Blog page breadcrumbs
	   	jQuery('.breadcrumbs').insertBefore('.is-blog.wordpress-homepage-view .post-list-wrapper');
	   	jQuery('.breadcrumbs').insertBefore('.is-blog.wordpress-post-view .post-view');
	   	// Blog video page breadcrumbs
	   	jQuery('.breadcrumbs').insertBefore('.is-blog.wordpress-term-view .post-list-wrapper');

	   	// Blog page Scroll Animation
	   	//new WOW().init();

		/*  category filters
		============================   */
	    size_li = $(".cat_landing_filter ol.items li").size();
	    x=15;
	    $('.cat_landing_filter ol.items li:lt('+x+')').show();
	    if(size_li <= x){
	        $('.m-show-more-action').hide();
	    }
	    $('.m-show-more-action').click(function () {
	        $('.cat_landing_filter ol.items li').show();
	        $('.m-show-less-action').show();
	        $(this).hide();
	    });
	    $('.m-show-less-action').click(function () {
	        $('.cat_landing_filter ol.items li').not(':lt('+x+')').hide();
	        $('.m-show-more-action').show();
	        $(this).hide();
	    });
	    

	    /* Home Page Banner slider action function*/
	    jQuery(document).on('click','.homeslider.part_finder',function(){
            //var varurl = jQuery('.homeslider a').attr('href');
            var varurl = jQuery(this).children('a').attr('href');
            if (varurl !== null && varurl !== undefined)
            {
                window.location.href = varurl;
            }
        });


        /* flying cart */
        
        /*jQuery(".related_content .related_qty_sect button.addalltocart").on('click', function () {
             var cart = jQuery('table.cart_section tbody tr');
             var imgtodrag = jQuery(this).parents(".related_qty_sect").find("button.addalltocart").eq(0);
             if (imgtodrag) {
                 var imgclone = imgtodrag.clone()
                 .offset({
                     top: imgtodrag.offset().top,
                     left: imgtodrag.offset().left
                 })
                 .css({
                    'opacity': '0.8',
                    'position': 'absolute',
                    'height': '37px',
                    'width': '150px',
                    'bottom': 'auto',
                    'z-index': '100', 
                    'background': '#9b0005',
                    'color': '#fff'
                 })
                 .appendTo(jQuery('body'))
                 .animate({
                    'top': cart.offset().top + 10,
                    'left': cart.offset().left + 10,
                    'width': '120px',
                    'height': '34px',
                    'padding': '5px'
                 }, 1000, 'easeInOutExpo');
                 imgclone.animate({
                     'width': 0,
                     'height': 0
                 }, function () {
                     jQuery(this).detach()
                 });
             }
        });*/
	});
})